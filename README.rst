===================================
 Lionbridge onDemand File Analyzer
===================================

This package is used by `Lionbridge onDemand`_ for some portions of file
analyzation. It is geared toward speed-and-ballpark rather than exact accuracy.
You likely will not benefit from this tool unless you are trying to emulate
onDemand analyzing for your own testing purposes.

.. _Lionbridge onDemand: https://ondemand.lionbridge.com


Overview
--------

The main entry point of this package is the :code:`analyze()` method. It accepts
a file path as an argument and will attempt to analyze the file using our default
analyzers and extractors. If we do not have an analyzer defined for that file type,
it will raise a :code:`FileExtensionNotSupportedException`. If the file type isn't
supported, you can supply your own analyzer and extractor.


Analyzers
=========

Analyzers take information from extractors and perform the actual unit counting. 
These analyzers contain an :code:`analyze()` method that accepts a file path
and returns an `Analysis`_ object.

All file paths are expected to be text (unicode). If they are bytes, they
will be decoded using the utf-8 encoding.


Extractors
==========

Extractors open files and convert the file contents into a format that can be
consumed by an analyzer. The method to open files and the format will vary by
analyzer.

Analysis
========

Analysis objects are returned from analyzers and contain the information that
was found during analysis. The following information can be found on an
analysis:

    * filename - the file that was analyzed
    * analyzer - the analyzer used
    * extractor - the extractor used
    * extension - extension of the file that was analyzed
    * errors - a list of errors that occurred
    * words - the number of words found
    * minutes - the number of minutes found
    * pages - the number of pages found in the
    * standardized_pages - the number of standardized pages found
    * rows - the number of rows found (excel files, for example)
    * characters - the number of characters found
    * files - the number of files analyzed
    * analyses - a list of analysis objects if the original file was an archive

The following properties can be used to get more information about an analysis:

    * analysis.successful - returns :code:`True` if no errors were found
    * analysis.has_analyses - returns :code:`True` if this analysis contains sub analyses (a zip file, for example)


Dependencies
------------

There are two things that should be pointed out:

1. The FFVideo package requires some system packages to be installed. For
   information on this see https://pypi.python.org/pypi/FFVideo.
2. NLTK data is required for some parsing, so you need to download that if you
   have not already.

.. code-block:: bash

    $ python -m nltk.downloader punkt


Basic Usage
-----------

.. code-block:: python

    >>> from liox_analyzer import file_analyzer
    >>> analysis = file_analyzer.analyze(file_path='/tmp/file.txt')
    >>> analysis.successful
    True
    >>> analysis.words
    20


CLI
---

A CLI can be also be used to analyze files.

Installation
============

.. code-block:: bash

    $ pip install .

Depending on the file types you will be analyzing, you may need to install extra
dependencies. The following extras are available:

* documents
* multimedia
* software
* xml

If you'd like to install all of the extra dependencies you can simply pass ``[extras]``.


.. code-block:: bash

    $ pip install .[extras]

If you only need to count words, you can pass
``[documents,software,xml]``.

.. code-block:: bash

    $ pip install .[documents,software,xml]


Example usage
=============

To determine if a file can be analyzed, simply pass a file path to the CLI.

.. code-block:: bash

    $ analyzer_cli ~/test.txt
    Analysis successful.

To view information about unit counts that were found during analysis, add the
``--counts`` flag.

.. code-block:: bash

    $ analyzer_cli --counts ~/test.txt
    words: 19
    characters: 20
    pages: 0
    standardized pages: 0
    minutes: 0
    rows: 4

An optional ``--locale`` argument may be passed for a more specific analysis.
If an Asian locale code is used, the CLI will use average characters per word
to determine words. If no locale is supplied, the CLI will assume ``en-us``.

.. code-block:: bash

    $ analyzer_cli --counts --locale zh-cn ~/test.txt
    words: 15
    characters: 20
    pages: 0
    standardized pages: 0
    minutes: 0
    rows: 4

To view the text that was extracted by our analyzer, add the ``--extract`` flag.
    
.. code-block:: bash

    $ analyzer_cli --extract ~/test.txt
    This is a test file.

    This is a test file's second line.

    This is still a test file.

If an error occurs while analyzing, an error message will be returned.

.. code-block:: bash

    $ analyzer_cli --counts ~/invalid_file.json
    Error: Parse error on line 8, column 43
    Invalid control character %r at:     "picture": "http://placehold.it/32x32,

    --^


Running Tests
-------------

Via tox:

.. code-block:: bash

    $ pip install tox
    $ tox