# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors.pdf import PDFExtractor


logger = logging.getLogger(__name__)


class PDFAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'pdf-analyzer'
    name = 'PDF Analyzer'
    desc = 'This analyzer handles PDF files.'
    default_extractor = PDFExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes a PDF file.

        This analyzers extractor should return a dictionary with the following
        keys:
        {
            'pages': ,
            'text': ,
        }

        Returns an Analysis object.
        """ 
        try:
            extracted_units = self.extractor.extract(file_path)
            pages = extracted_units[Analysis.UNIT_TYPE_PAGES]
            text = extracted_units['text']
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            units = self.get_units_from_text(text)
            units[Analysis.UNIT_TYPE_PAGES] = pages
            units[Analysis.UNIT_TYPE_STANDARDIZED_PAGES] = pages
            self.analysis.set_units(units)

        return self.analysis


