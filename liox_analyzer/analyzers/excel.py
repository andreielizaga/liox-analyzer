# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors.file_content import FileContentExtractor


logger = logging.getLogger(__name__)


class ExcelAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'excel-analyzer'
    name = 'Excel Analyzer'
    desc = 'An analyzer to gather information from excel files.'
    default_extractor = FileContentExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        try:
            import xlrd
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'try running `pip install liox_file_analyzer[documents]`.'
            )
        else:
            rows = 0
            pages = 0
            text = u''
            try:
                extracted_data = self.extractor.extract(file_path)
                file_content = extracted_data.get('text')
                wb = xlrd.open_workbook(file_contents=file_content)

                for sheet in wb.sheets():
                    # 0 = visible
                    # 1 = hidden (can be unhidden by user -- Format -> Sheet -> Unhide)
                    # 2 = "very hidden" (can be unhidden only by VBA macro).
                    if sheet.visibility != 0:
                        continue
                    pages += 1
                    for i in range(sheet.nrows):
                        try:
                            row_text = u' ' + (
                                u'. '.join(u'{}'.format(cell) for cell in sheet.row_values(i))
                            ) + u'.\n'
                            # only counting the row if there is some data in it.
                            if len(row_text.strip()) > 0:
                                rows += 1
                            text += row_text
                        except Exception:
                            logger.exception('Error processing excel cells')
                            raise LioxAnalyzerError(file_path)
            except LioxAnalyzerError as e:
                self.errors.append(e)

            if self.errors:
                self.analysis.errors = self.errors
            else:
                units = self.get_units_from_text(text)
                units[Analysis.UNIT_TYPE_ROWS] = rows
                units[Analysis.UNIT_TYPE_PAGES] = pages
                self.analysis.set_units(units)

        return self.analysis

