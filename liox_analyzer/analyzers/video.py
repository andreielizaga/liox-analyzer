# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..extractors.video import VideoExtractor


logger = logging.getLogger(__name__)


class VideoAnalyzer(Analyzer):
    id = 'video-analyzer'
    name = 'Video Analyzer'
    desc = 'This analyzer handles video files'
    default_extractor = VideoExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes a video file. This implementation only cares about the video
        file's duration.

        This analyzer expects the extractor to return the video's duration in
        seconds.

        Returns an Analysis object.
        """
        units = Analysis.empty_unit_type_dict()

        try:
            duration = self.extractor.extract(file_path).get(Analysis.UNIT_TYPE_MINUTES)
            units[Analysis.UNIT_TYPE_MINUTES] = duration
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            self.analysis.set_units(units)

        return self.analysis
