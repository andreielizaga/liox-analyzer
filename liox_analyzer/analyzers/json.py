# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors.json import JSONExtractor


logger = logging.getLogger(__name__)


class JSONAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'json-analyzer'
    name = 'JSON Analyzer'
    desc = 'An analyzer to gather information about a JSON file.'
    default_extractor = JSONExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes a JSON file. The JSON file is converted into a single string
        that is then analyzed.

        Returns an Analysis object.
        """
        try:
            text = self.extractor.extract(file_path).get('text')
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            units = self.get_units_from_text(text)
            self.analysis.set_units(units)

        return self.analysis


