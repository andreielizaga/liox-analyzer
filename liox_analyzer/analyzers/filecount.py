# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from ..analysis import Analysis  # pylint: disable=unused-import


logger = logging.getLogger(__name__)


class FileCountAnalyzer(Analyzer):
    id = 'file-count-analyzer'
    name = 'File Count Analyzer'
    desc = 'Counts how many files were passed.'

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        This analyzer simply creates an analysis object that returns how many
        files were passed to it.

        Returns an Analysis object.
        """
        units = Analysis.empty_unit_type_dict()
        
        units[Analysis.UNIT_TYPE_PAGES] = 1
        units[Analysis.UNIT_TYPE_STANDARDIZED_PAGES] = 1

        self.analysis.set_units(units)

        return self.analysis



