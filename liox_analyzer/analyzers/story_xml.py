# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..extractors.story_xml import StoryXMLExtractor
from ..exceptions import LioxAnalyzerError
from ..analysis import Analysis
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class StoryXMLAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'story-xml-analyzer'
    name = 'Story XML Analyzer'
    desc = 'An analyzer to gather information from XML files within a story file.'
    default_extractor = StoryXMLExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        XML files generated from inside a story archive contain meta data that should not be
        included in the translations. This is a custom xml analyzer for story files which extracts
        translatable texts from the different story file generator softwares - namely "Storyist" &
        "Articulate Storyline". Each of these have different xml attribute formats.

        Returns an Analysis object.
        """
        text = u''

        try:
            xml_dom = self.extractor.extract(file_path).get('text')
        except LioxAnalyzerError as e:
            self.errors.append(e)
        else:
            if xml_dom.documentElement.tagName == 'office:document':
                text = remove_markup_tags(' '.join([_dom.toxml() for _dom in \
                        xml_dom.getElementsByTagNameNS('*', 'text')]))
            elif '/story/slides/' in file_path:
                # Slides
                text = u' '.join([_dom.firstChild.nodeValue for _dom in \
                        xml_dom.getElementsByTagName('plain') if _dom.firstChild])

                # Notes
                note_text = u' '.join([_dom.firstChild.nodeValue for _dom in \
                        xml_dom.getElementsByTagName('note') if _dom.firstChild])

                if note_text:
                    try:
                        from bs4 import BeautifulSoup
                    except ImportError:
                        logger.error(
                            'You need to install additional dependencies for '
                            'to work. Try running `pip install '
                            'liox_file_analyzer[xml]`'
                        )
                        raise
                    # Extract the note text from span text attributes
                    soup = BeautifulSoup(note_text, 'xml')
                    text += u' '.join([content.get('Text', '').strip() for \
                            content in soup.find_all('Span')])
            else:
                units = Analysis.empty_unit_type_dict()
                self.analysis.set_units(units)
                return self.analysis

        if self.errors:
            self.analysis.errors = self.errors
        elif text:
            units = self.get_units_from_text(text)
            units[Analysis.UNIT_TYPE_PAGES] = 1
            units[Analysis.UNIT_TYPE_STANDARDIZED_PAGES] = 1
            self.analysis.set_units(units)
        else:
            units = Analysis.empty_unit_type_dict()
            self.analysis.set_units(units)

        return self.analysis

