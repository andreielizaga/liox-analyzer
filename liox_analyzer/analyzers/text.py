# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..extractors.multi_codec import MultipleCodecExtractor


logger = logging.getLogger(__name__)


class TextAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'text-analyzer'
    name = 'Text Analyzer'
    desc = 'An analyzer to gather information from text files.'
    default_extractor = MultipleCodecExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes extracted text from a file and returns unit information.

        Returns an Analysis object.
        """
        try:
            extracted_data = self.extractor.extract(file_path)
            text = extracted_data.get('text')
            text = text.strip('\0')
            rows = max(len(text.splitlines()) - 1, 0)
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            units = self.get_units_from_text(text)
            units[Analysis.UNIT_TYPE_ROWS] = rows
            self.analysis.set_units(units)

        return self.analysis


