# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import logging

from abc import ABCMeta, abstractmethod
from six import python_2_unicode_compatible, add_metaclass
from typing import List, Optional, Sequence, Type  # pylint: disable=unused-import

from ..analysis import Analysis  # pylint: disable=unused-import
from ..extractors.base import Extractor  # pylint: disable=unused-import


logger = logging.getLogger(__name__)


@python_2_unicode_compatible
@add_metaclass(ABCMeta)
class Analyzer(object):
    id = 'file-analyzer'
    name = 'File Analyzer'
    desc = None  # type: str
    file_path = None  # type: str
    errors = None  # type: List[Exception]
    default_extractor = None  # type: Optional[Type[Extractor]]
    extractor = None  # type: Extractor

    def __init__(self, extractor=None):
        # type: (Type[Extractor]) -> None
        self.errors = []

        if extractor:
            self.extractor = extractor()
        elif self.default_extractor:
            self.extractor = self.default_extractor()

        self.analysis = Analysis()
        self.analysis.analyzer = str(self)
        self.analysis.extractor = str(self.extractor)

        super(Analyzer, self).__init__()

    def __str__(self):
        # type: () -> str
        return self.name

    def __repr__(self):
        # type: () -> str
        return '<{}: {}>'.format(self.name, self.id)

    @abstractmethod
    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        All work done to analyze extracted file information should be done in
        this method. Results should be stored on the analyzer's analysis attribute.

        Returns the analyzer's analysis object
        """
