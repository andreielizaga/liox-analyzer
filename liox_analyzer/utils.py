# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import os
import math

from itertools import chain
from typing import Generic, Tuple, Type, TypeVar  # pylint: disable=unused-import

from .analyzers.base import Analyzer  # pylint: disable=unused-import

from .analyzers import (
    AudioAnalyzer,
    BasicAnalyzer,
    ExcelAnalyzer,
    FileCountAnalyzer,
    JSONAnalyzer,
    PDFAnalyzer,
    PowerpointAnalyzer,
    StoryAnalyzer,
    TextAnalyzer,
    VideoAnalyzer,
    ZipAnalyzer,
)

from . import constants as const
from .exceptions import FileExtensionNotSupportedError

from .extractors.base import Extractor  # pylint: disable=unused-import
from .extractors import (
    CatdocExtractor,
    DocxExtractor,
    EpubExtractor,
    ErbExtractor,
    HtmlExtractor,
    IdmlExtractor,
    InxExtractor,
    MifExtractor,
    OdfExtractor,
    PoExtractor,
    PropertiesExtractor,
    PsdExtractor,
    ResExtractor,
    RtfExtractor,
    SrtExtractor,
    StringsExtractor,
    TmxExtractor,
    VttExtractor,
    XlfExtractor,
    XmlExtractor,
    XmlExcludeNoTranslateExtractor,
    YamlExtractor,
    FltocExtractor,
    PHPExtractor,
)
from .text_utils import smart_bytes


def determine_analyzer_and_extractor(file_path):
    # type: (str) -> Tuple[Type[Analyzer], Type[Extractor]]
    """
    Utility function to return the analyzer and extractor associated with
    the supplied file path.
    """
    analyzer = determine_analyzer(file_path)
    extractor = determine_override_extractor(file_path)
    if extractor is None:
        extractor = analyzer.default_extractor
    return analyzer, extractor


def determine_analyzer(file_path):
    # type: (unicode) -> Type[Analyzer]
    """
    Utility function to decide which analyzer should be used based on the
    extension of a file.

    Raises a FileExtensionNotSupportedError if a match isn't found.
    """
    extension = extension_from_filename(file_path).lower()
    if extension in const.JSON_FILE_TYPES:
        return JSONAnalyzer
    if extension in const.TEXT_FILE_TYPES:
        return TextAnalyzer
    if extension in const.AUDIO_FILE_TYPES:
        return AudioAnalyzer
    if extension in const.EXCEL_FILE_TYPES:
        return ExcelAnalyzer
    if extension in const.PDF_FILE_TYPES:
        return PDFAnalyzer
    if extension in const.POWERPOINT_FILE_TYPES:
        return PowerpointAnalyzer
    if extension in const.VIDEO_FILE_TYPES:
        return VideoAnalyzer
    if extension in const.IMAGE_FILE_TYPES:
        return FileCountAnalyzer
    if extension in const.ARCHIVE_FILE_TYPES:
        return StoryAnalyzer
    if extension in const.ZIP_FILE_TYPES:
        return ZipAnalyzer
    if extension in chain(*const.BASIC_ANALYZER_FILE_TYPES):
        return BasicAnalyzer
    raise FileExtensionNotSupportedError(file_path)


def determine_override_extractor(file_path):
    # type: (unicode) -> Type[Extractor]
    """
    Utility funciton to decide if another extractor should be used instead of
    an analyzer's default.

    Returns None if no overrides are found.
    """
    extension = extension_from_filename(file_path).lower()
    if extension in const.DOC_FILE_TYPES:
        return CatdocExtractor
    if extension in const.DOCX_FILE_TYPES:
        return DocxExtractor
    if extension in const.RES_FILE_TYPES:
        return ResExtractor
    if extension in const.TMX_FILE_TYPES:
        return TmxExtractor
    if extension in const.PO_FILE_TYPES:
        return PoExtractor
    if extension in const.XLF_FILE_TYPES:
        return XlfExtractor
    if extension in const.XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES:
        return XmlExcludeNoTranslateExtractor
    if extension in const.XML_INCLUDE_NO_TRANSLATE_FILE_TYPES:
        return XmlExtractor
    if extension in const.ODF_FILE_TYPES:
        return OdfExtractor
    if extension in const.YAML_FILE_TYPES:
        return YamlExtractor
    if extension in const.PROPERTIES_FILE_TYPES:
        return PropertiesExtractor
    if extension in const.STRINGS_FILE_TYPES:
        return StringsExtractor
    if extension in const.SRT_FILE_TYPES:
        return SrtExtractor
    if extension in const.VTT_FILE_TYPES:
        return VttExtractor
    if extension in const.MIF_FILE_TYPES:
        return MifExtractor
    if extension in const.ERB_FILE_TYPES:
        return ErbExtractor
    if extension in const.HTML_FILE_TYPES:
        return HtmlExtractor
    if extension in const.IDML_FILE_TYPES:
        return IdmlExtractor
    if extension in const.RTF_FILE_TYPES:
        return RtfExtractor
    if extension in const.INX_FILE_TYPES:
        return InxExtractor
    if extension in const.PSD_FILE_TYPES:
        return PsdExtractor
    if extension in const.EPUB_FILE_TYPES:
        return EpubExtractor
    if extension in const.FLTOC_FILE_TYPES:
        return FltocExtractor
    if extension in const.PHP_FILE_TYPES:
        return PHPExtractor
    return None


def extension_from_filename(filename):
    # type: (unicode) -> str
    """
    Return a file's extension without the '.'
    """
    return smart_bytes(os.path.splitext(filename)[1][1:])


def content_type_from_extension(extension):
    """
    Returns the content type based from extension

    >>> content_type_from_extension('txt')
    'text/plain'
    >>> content_type_from_extension('json')
    'application/json'
    """
    extension = extension.lower()
    for ct, ext_list in const.SUPPORTED_CONTENT_TYPES.items():
        if extension in ext_list:
            return ct
    return None


def decode_media_type(media_type, file_extension):
    """
    Takes a media type like 'text/plain' and returns a file type that we use
    for parsing the file, like 'doc'.
    """
    file_extension = file_extension.lower()
    if 'video' in media_type:
        return 'video'
    elif any(ext in media_type for ext in ['presentation', 'ms-powerpoint', 'pdf']):
        # .pot is an extension for both PowerPoint files and text translation
        # files. We do not support the former. But if presentation software is
        # installed on the user's computer and they upload a .pot translation
        # file it may be sent with a media type like 'application/ms-powerpoint'.
        # So we have a special case here to be sure that .pot files are treated
        # as doc and not slides.
        if file_extension in ['pot']:
            return 'doc'
        return 'slides'
    elif 'image' in media_type:
        return 'image'
    elif 'zip' in media_type:
        return 'zip'
    elif 'audio' in media_type:
        return 'audio'
    else:
        return 'doc'


def media_type_from_extension(extension):
    """
    Returns a media type based on an extension.
    Uses supported file types constants to make the determination
    >>> media_type_from_extension('txt')
    'doc'
    >>> media_type_from_extension('txt')
    'doc'
    """
    extension = extension.lower()
    if extension in const.SUPPORTED_VIDEO_FILE_TYPES:
        return 'video'
    elif extension in const.SUPPORTED_TEXT_FILE_TYPES:
        return 'doc'
    elif extension in const.SUPPORTED_AUDIO_FILE_TYPES:
        return 'audio'
    elif extension in const.SUPPORTED_SLIDE_FILE_TYPES:
        return 'slides'
    elif extension in const.IMAGE_FILE_TYPES:
        return 'image'
    elif extension in const.SUPPORTED_ARCHIVE_FILE_TYPES:
        return 'archive'
    else:
        return ''


def is_video_file(extension):
    """
    Returns a boolean and checks if the extension is a video file

    >>> is_video_file('mp4')
    True
    >>> is_video_file('txt')
    False
    """
    return 'video' == media_type_from_extension(extension)


def is_audio_file(extension):
    """
    Returns a boolean that checks if the extension is an audio file

    >>> is_audio_file('wav')
    True
    >>> is_audio_file('mov')
    False
    """
    return 'audio' == media_type_from_extension(extension)


def is_doc_file(extension):
    """
    Returns a boolean that checks if the extension a document file

    >>> is_doc_file('txt')
    True
    >>> is_doc_file('jpg')
    False
    """
    return 'doc' == media_type_from_extension(extension)


def is_archive_file(extension):
    """
    Returns a boolean whether the extension is a zip

    >>> is_archive_file('zip')
    True
    >>> is_archive_file('txt')
    False
    """
    return 'archive' == media_type_from_extension(extension)


def is_slides_file(extension):
    """
    Returns a boolean whether the extension is a ppt/pptx/pdf

    >>> is_slides_file('pdf')
    True
    >>> is_slides_file('txt')
    False
    """
    return 'slides' == media_type_from_extension(extension)


def is_image_file(extension):
    """
    Returns a boolean whether the extension is an image file

    >>> is_image_file('png')
    True
    >>> is_image_file('mov')
    False
    """
    return 'image' == media_type_from_extension(extension)


def extension_supported(extension):
    """
    Returns a boolean and checks if an extension is supported

    >>> extension_supported('txt')
    True
    >>> extension_supported('non-txt')
    False
    """
    if not media_type_from_extension(extension):
        return False
    return True


def characters_per_page(locale):
    if locale == 'zh':  # Chinese
        return 833
    elif locale == 'ko':  # Korean
        return 454.5
    elif locale == 'ja':  # Japanese
        return 500
    elif locale == 'th':  # Thai
        return 250
    else:  # Latin
        return 1500


def count_asian_words(language_code, characters):
    """ Returns number of words for asian language """
    # If language is Asian (japanese, chinese, thai, korean)
    return int(math.ceil(characters/const.ASIAN_LANGUAGES[language_code]))


def get_minutes(seconds):
    """ Returns a convertion of seconds to minutes. """
    return int(math.ceil(seconds / 60))
