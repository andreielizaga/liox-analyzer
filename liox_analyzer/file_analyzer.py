# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import os

from typing import Type  # pylint: disable=unused-import

from .analysis import Analysis  # pylint: disable=unused-import
from .analyzers.base import Analyzer  # pylint: disable=unused-import
from .extractors.base import Extractor  # pylint: disable=unused-import
from .utils import (
    determine_analyzer,
    determine_override_extractor,
    extension_from_filename,
)
from .text_utils import smart_text

logger = logging.getLogger(__name__)


def analyze(file_path, analyzer=None, extractor=None):
    # type: (str, Type[Analyzer], Type[Extractor]) -> Analysis
    """
    Accepts a file path that will be analyzed. We will choose an analyzer /
    extractor based on the file's extension, but one may be supplied instead.

    Returns an Analysis object containing results from the analyzer.
    """
    kwargs = {}

    file_path = smart_text(file_path)

    if extractor is None:
        extractor = determine_override_extractor(file_path) or None

    if extractor is not None:
        kwargs['extractor'] = extractor

    if analyzer is None:
        analyzer = determine_analyzer(file_path)

    analysis = analyzer(**kwargs).analyze(file_path)

    filename = os.path.basename(file_path)
    extension = extension_from_filename(file_path)

    analysis.extension = extension
    analysis.filename = filename

    return analysis
