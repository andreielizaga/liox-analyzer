# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import argparse
import logging
import sys

from liox_analyzer import file_analyzer
from . import constants
from .utils import (
    count_asian_words,
    determine_analyzer,
    determine_override_extractor,
)


logging.basicConfig(level=logging.CRITICAL)
logging.getLogger().addHandler(logging.StreamHandler())


def main():
    # type: () -> None
    """
    A simple CLI for using the analyzer.

    Accepts the following arguments:
        --counts: optional flag that will print unit counts if supplied
        --locale: optional string argument that will use average characters per
            word if an Asian language code is supplied
        --extract: optional flag that will print text that was extracted from
            the file to be used in analysis
        file_path: required string argument that points to the file that will
            be analyzed
    """
    parser = argparse.ArgumentParser(description='Liox Analyzer CLI')

    parser.add_argument('--counts', action='store_true', default=False)
    parser.add_argument('--locale', action='store', dest="locale", default='en-us')
    parser.add_argument('--extract', action='store_true', default=False)
    parser.add_argument('file_path', action='store')

    options = parser.parse_args()
    file_path = options.file_path
    counts = options.counts
    locale = options.locale
    extract = options.extract

    try:
        analysis = file_analyzer.analyze(file_path=file_path)
    except ImportError:
        sys.exit(1)

    if analysis.successful:
        if not counts and not extract:
            print('Analysis successful.')

        if counts:
            words = analysis.words
            if locale in constants.ASIAN_LANGUAGES:
                words = count_asian_words(locale, analysis.characters) 

            print('words: {}'.format(words))
            print('characters: {}'.format(analysis.characters))
            print('pages: {}'.format(analysis.pages))
            print('standardized pages: {}'.format(analysis.standardized_pages))
            print('minutes: {}'.format(analysis.minutes))
            print('rows: {}'.format(analysis.rows))

        if extract:
            extractor = determine_override_extractor(file_path) or None
            kwargs = {}
            if extractor is not None:
                kwargs['extractor'] = extractor
            analyzer = determine_analyzer(file_path)(**kwargs)
            text = analyzer.extractor.extract(file_path).get('text')
            print(text)

        sys.exit(0)

    else:
        for error in analysis.errors:
            print(error)

        sys.exit(1)


if __name__ == '__main__':
    main()
