# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import six

from itertools import chain
from typing import Dict, List, Type, Union  # pylint: disable=unused-import

logger = logging.getLogger(__name__)


@six.python_2_unicode_compatible
class Analysis(object):
    UNIT_TYPE_WORDS = 'words'
    UNIT_TYPE_MINUTES = 'minutes'
    UNIT_TYPE_PAGES = 'pages'
    UNIT_TYPE_STANDARDIZED_PAGES = 'standardized_pages'
    UNIT_TYPE_ROWS = 'rows'
    UNIT_TYPE_CHARACTERS = 'characters'
    UNIT_TYPE_FILES = 'files'
    UNIT_TYPE_EXCERPT = 'excerpt'
    UNIT_TYPE_SLIDES_WORDS = 'slides_words'
    UNIT_TYPE_NOTES_WORDS = 'notes_words'

    UNIT_TYPES = (
        UNIT_TYPE_WORDS,
        UNIT_TYPE_MINUTES,
        UNIT_TYPE_PAGES,
        UNIT_TYPE_STANDARDIZED_PAGES,
        UNIT_TYPE_ROWS,
        UNIT_TYPE_CHARACTERS,
        UNIT_TYPE_FILES,
        UNIT_TYPE_EXCERPT,
        UNIT_TYPE_SLIDES_WORDS,
        UNIT_TYPE_NOTES_WORDS,
    )

    filename = None  # type: str
    analyzer = None  # type: str
    extractor = None  # type: str
    extension = None  # type: str
    _errors = None  # type: List[Exception]
    words = None  # type: int
    minutes = None  # type: int
    pages = None  # type: int
    standardized_pages = None  # type: int
    rows = None  # type: int
    characters = None  # type: int
    files = None  # type: int
    excerpt = None  # type: str
    analyses = None  # type: List[Analysis]
    slides_words = None  # type: int
    notes_words = None  # type: int

    def __init__(self):
        # type: () -> None
        self._errors = []
        self.analyses = []
        super(Analysis, self).__init__()

    def __str__(self):
        # type: () -> str
        return 'Analysis of File <{}>'.format(self.filename)

    @classmethod
    def empty_unit_type_dict(cls):
        # type: () -> Dict[str, Union[int, str]]
        """
        Simple class method to create a unit type dictionary out of all of the
        possible unit types.
        """
        unit_type_dict = {}  # type: Dict[str, Union[int, str]]

        for unit_type in cls.UNIT_TYPES:
            unit_type_dict[unit_type] = 0

        unit_type_dict[cls.UNIT_TYPE_EXCERPT] = ''

        return unit_type_dict

    @property
    def successful(self):
        # type: () -> bool
        if self.has_analyses:
            return all(analysis.successful for analysis in self.analyses)
        return len(self.errors) == 0
    
    @property
    def has_analyses(self):
        # type: () -> bool
        return len(self.analyses) > 0

    @property
    def errors(self):
        # type: () -> List
        if self.has_analyses:
            sub_errors = [analysis.errors for analysis in self.analyses if analysis.errors]
            return list(chain(*sub_errors))
        return self._errors

    @errors.setter
    def errors(self, value):
        # type: (List[Exception]) -> None
        self._errors = value

    def set_units(self, units):
        # type: (Dict) -> None
        """
        Method used to set unit values as attributes on a particular analysis.

        The units argument should be a dictionary created via
        Analysis.empty_unit_type_dict. 
        """
        for key, value in units.items():
            setattr(self, key, value)

    def sum_analyses(self):
        # type: () -> Dict
        """
        Method to sum up the units gathered for analyses that may be included
        in this analysis. Archive type files will generally have sub analyses.

        Returns a dictionary containing unit information.
        """
        units = self.empty_unit_type_dict()
        if self.has_analyses:
            for key in units.keys():
                for analysis in self.analyses:
                    units[key] += getattr(analysis, key)
        return units

