# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import os

from typing import Any, List, Optional  # pylint: disable=unused-import


class LioxAnalyzerError(Exception):

    def __init__(self, file_path, msg=None):
        # type: (unicode, Optional[str]) -> None
        if msg is None:
            msg = 'An error occurred while analyzing.' 
        if file_path is None:
            raise ValueError('file_path cannot be `None`')
        self.file_path = file_path
        super(LioxAnalyzerError, self).__init__(msg)

    @property
    def file_extension(self):
        # type: () -> unicode
        from .utils import extension_from_filename
        return extension_from_filename(self.file_path)


class FileExtensionNotSupportedError(LioxAnalyzerError):
    pass


class InvalidFileError(LioxAnalyzerError):
    error_message_format = \
        'Error: Parse error on line {lineno}, column {colno}\n' \
        + '{message}: {errant_line}\n' \
        + '--^'  # type: str

    def __init__(self, lineno, colno, msg, errant_line, file_path):
        # type: (int, int, str, int, unicode) -> None
        self.lineno = lineno
        self.colno = colno
        self.errant_line = errant_line
        self.msg = msg
        self.filename = os.path.basename(file_path)
        super(InvalidFileError, self).__init__(file_path, msg=self.__str__())

    def __str__(self):
        # type: () -> str
        return self.error_message_format.format(
            lineno=self.lineno,
            colno=self.colno,
            message=self.msg,
            errant_line=self.errant_line
        )

class VideoNotValidError(LioxAnalyzerError):
    pass


class VideoDurationError(LioxAnalyzerError):
    pass


class LioxUnicodeDecodeError(UnicodeDecodeError, LioxAnalyzerError):
    def __init__(self, obj, *args):
        # type: (Any, *Any) -> None
        self.obj = obj
        super(LioxUnicodeDecodeError, self).__init__(*args)

    def __str__(self):
        # type: () -> str
        original = UnicodeDecodeError.__str__(self)
        return '%s. You passed in %r (%s)' % (original, self.obj, type(self.obj))
