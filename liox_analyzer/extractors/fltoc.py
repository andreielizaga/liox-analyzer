# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class FltocExtractor(Extractor):
    id = 'fltoc-extractor'
    name = 'FLTOC File Extractor'
    desc = 'Extracts text from an FLTOC file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an FLTOC File.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()
            soup = BeautifulSoup(file_content, 'xml')

            # Each TocEntry element corresponds to a topic file. The relative file location is
            # indicated by the Link attribute and the Title attribute takes the value of the
            # title element in the topic.
            soup = BeautifulSoup(file_content, 'xml')
            text = ' '.join([content.get('Title', '').strip() for content in \
                            soup.find_all('TocEntry')])

            return {
                    'text': text,
                }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
