# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class RtfExtractor(Extractor):
    id = 'rtf-extractor'
    name = 'Rich Text Format File Extractor'
    desc = 'Extracts text from an rtf file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an RTF file.
        """
        try:
            from pyth.plugins.rtf15.reader import Rtf15Reader
            from pyth.plugins.plaintext.writer import PlaintextWriter
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_:
                doc = Rtf15Reader.read(file_)

            return {
                'text': PlaintextWriter.write(doc).getvalue(),
            }   
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)




