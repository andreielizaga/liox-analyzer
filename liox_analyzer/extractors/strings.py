# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict, Optional  # pylint: disable=unused-import

from .base import Extractor
from .mixins import MultiCodecExtractorMixin
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class StringsExtractor(Extractor, MultiCodecExtractorMixin):
    id = 'strings-extractor'
    name = 'Strings File Extractor'
    desc = 'Extracts text from a Strings file.'

    def extract(self, file_path, encoding=None):
        # type: (unicode, Optional[str]) -> Dict
        """
        Extracts the text from a Strings file.
        """
        try:
            text = ''

            file_content = self.multi_codec_read_file(file_path, encoding)

            # stripping out comments
            text = re.sub(r'/\*.*\*/', '', file_content)

            # get values of key value pairs
            text = '. '.join(re.findall(r'=\s?\"(.*)\";', text))

            # exclude placeholders
            text = re.sub(r'(%[^%\s,.();:\"\'!?]+)', '', text)

            text = remove_markup_tags(text)

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)






