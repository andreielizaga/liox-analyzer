# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from .mixins import PowerpointMixin
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PowerpointPagesExtractor(Extractor, PowerpointMixin):
    id = 'powerpoint-pages-extractor'
    name = 'Powerpoint Page Extractor'
    desc = 'Extracts pages from a Powerpoint file using the pptx library.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the number of pages contained in the powerpoint file.
        """
        presentation = self.create_presentation(file_path)

        try:
            return {
                Analysis.UNIT_TYPE_PAGES: len(presentation.slides),
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)


