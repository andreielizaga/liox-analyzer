# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict, Optional  # pylint: disable=unused-import

from .base import Extractor
from .mixins import MultiCodecExtractorMixin
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class TmxExtractor(Extractor, MultiCodecExtractorMixin):
    id = 'tmx-extractor'
    name = 'Resource File Extractor'
    desc = 'Extracts text from a TMX file using Beautiful Soup.'

    def extract(self, file_path, encoding=None):
        # type: (unicode, Optional[str]) -> Dict
        """
        Extracts the text from a Translation Memory Exchange.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            text = ''

            file_content = self.multi_codec_read_file(file_path, encoding)

            soup = BeautifulSoup(file_content)
            # Get source language which by default is located on header
            header = soup.find('header')
            source_language = header.get('srclang')

            tu = soup.find('tu')
            source_language = tu.get('srclang', source_language)

            for tuv in soup.find_all('tuv', {'xml:lang': source_language}):
                # build text from source segments, delimited by '.'
                text += ' '.join(['. ' + ' '.join(contents.stripped_strings) for contents in tuv.find_all('seg')])
            return {
                'text': text,
                Analysis.UNIT_TYPE_PAGES: 1,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)




