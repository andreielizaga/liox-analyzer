# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

# We need this import here for mypy annotations, but we're ignoring any import
# errors for now because we'll alert the user later on
try:
    from pptx import Presentation  # pylint: disable=unused-import
except ImportError:
    pass

from typing import List, Dict  # pylint: disable=unused-import

from .base import Extractor
from .mixins import PowerpointMixin
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PowerpointWordsExtractor(Extractor, PowerpointMixin):
    id = 'powerpoint-words-extractor'
    name = 'Powerpoint Words Extractor'
    desc = 'Extracts text from a Powerpoint file using the pptx library.'

    def _get_slide_text(self, paragraphs):
        # type: (List[Presentation]) -> str
        """
        Loops through paragraph runs from pptx text_frame.paragraphs and
        concatenates the text.
        """
        text = []
        for paragraph in paragraphs:
            for run in paragraph.runs:
                text.append(run.text)
        return ' '.join(text)

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts Words from a powerpoint file. This extractor only counts words
        on slides and doesn't include notes.
        """
        presentation = self.create_presentation(file_path)

        try:
            text = u''
            for slide in presentation.slides:
                for shape in slide.shapes:
                    if shape.has_text_frame:
                        # Get text found on a shape object in a slide.
                        text = u'{} {}'.format(text, self._get_slide_text(
                            shape.text_frame.paragraphs
                        ))

                    if shape.has_table:
                        # Get text from a Table inside the Shape object.
                        for row_count, _ in enumerate(shape.table.rows):
                            for column_count, _ in enumerate(shape.table.columns):
                                # Get the paragraphs from the specific table cell.
                                text = u'{} {}'.format(text, self._get_slide_text(
                                    shape.table.cell(
                                        row_count, column_count
                                    ).text_frame.paragraphs
                                ))
            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)



