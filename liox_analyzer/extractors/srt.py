# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class SrtExtractor(Extractor):
    id = 'srt-extractor'
    name = 'SRT File Extractor'
    desc = 'Extracts text from an SRT file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an SRT file.
        """
        try:
            text = ''

            # Stripping out the time stamps and condensing
            with open(file_path, 'r') as file_:
                file_content = file_.read()
            text = re.sub(r'\d+.*\n\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d', ' ', file_content)

            text = remove_markup_tags(text)

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)






