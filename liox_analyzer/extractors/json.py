# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Extractor
from ..exceptions import InvalidFileError, LioxAnalyzerError
from ..text_utils import get_string_values, remove_markup_tags


logger = logging.getLogger(__name__)


class JSONExtractor(Extractor):
    id = 'json-extractor'
    name = 'JSON Extractor'
    desc = 'Extracts text from a JSON file.'

    def extract(self, file_path):
        """
        # type: (unicode) -> Dict
        (mypy support pulled because of an issue with reading attributes on
        JSONDecodeError)

        Extracts text from a JSON file and converts the JSON dictionary into
        a string.

        Raises a JSONDecodeException if there was an error converting the
        JSON file.
        """
        try:
            import simplejson
            from simplejson.scanner import JSONDecodeError
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [software] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_:
                file_content = file_.read()
                json_data = simplejson.loads(file_content) 
                text = get_string_values(json_data)
                return {
                    'text': remove_markup_tags(text)
                }
        except JSONDecodeError as e:
            errant_line = None
            with open(file_path, 'r') as file_:
                errant_line = file_.readlines()[e.lineno - 1]
            logger.exception('An error occurred while extracting %s', file_path)
            raise InvalidFileError(e.lineno, e.colno, e.msg, errant_line, file_path)
        except Exception as e:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path, msg=e.message)
