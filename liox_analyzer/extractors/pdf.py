# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from .pdf_words import PDFWordExtractor
from .pdf_reader import PDFReaderExtractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PDFExtractor(Extractor):
    id = 'pdf-extractor'
    name = 'PDF Extractor'
    desc = 'Extracts various pieces of unit data from a pdf file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts different unit information from a pdf file. This
        extractor uses other extractors to gather data.

        Returns a dictionary with the followign keys:
            - pages
            - text
        """
        try:
            pages = PDFReaderExtractor().extract(file_path).get('pages')
            text = PDFWordExtractor().extract(file_path).get('text')
            return {
                Analysis.UNIT_TYPE_PAGES: pages,
                'text': text,
            }   
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)




