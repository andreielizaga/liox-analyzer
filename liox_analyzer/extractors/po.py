# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class PoExtractor(Extractor):
    id = 'po-extractor'
    name = 'Po File Extractor'
    desc = 'Extracts text from a po file.'

    def extract(self, file_path):
        """
        # type: (unicode) -> Dict
        (mypy support pulled because of an issue with reading attributes on
        UnicodeError and UnicodeDecodeError)

        Extracts the text from a PO file using polib
        """
        try:
            import polib
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [software] extras.'
            )
            raise
        try:
            supported_codecs = ('utf-8', 'ascii', 'utf-16', 'iso-8859-1', None)

            for codec in supported_codecs:
                try:
                    p = polib.pofile(file_path, encoding=codec)
                    text = '. '.join([e.msgid for e in p.translated_entries()])
                    text += '. '.join([e.msgid for e in p.untranslated_entries()])
                    break
                except UnicodeDecodeError as e:
                    msg = 'Decoding Error: {}. File: {}. String Error: {}'.format(
                        e.encoding, file_path, e.object)
                    logger.debug(msg)
                    raise e
                except UnicodeError as e:
                    msg = 'Unicode Error: {}. File: {}. String Error: {}'.format(
                        e.encoding, file_path, e.object)
                    logger.debug(msg)
                    raise e
            text = remove_markup_tags(text)
            return {
                'text': text,
            }   
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)




