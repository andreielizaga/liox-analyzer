# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import extract_from_markup


logger = logging.getLogger(__name__)


class HtmlExtractor(Extractor):
    id = 'html-extractor'
    name = 'HTML File Extractor'
    desc = 'Extracts text from an HTML file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an HTML File.
        """
        try:
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()

            text = extract_from_markup(file_content)

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
