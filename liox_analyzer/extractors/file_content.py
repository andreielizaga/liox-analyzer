# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class FileContentExtractor(Extractor):
    id = 'file-content-extractor'
    name = 'File Content Extractor'
    desc = 'Extracts text from a file by simply reading the contents.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text from a file simply using open()
        """
        try:
            with open(file_path, 'r') as file_:
                file_content = file_.read()
            return {
                'text': file_content,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)

