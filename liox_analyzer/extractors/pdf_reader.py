# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PDFReaderExtractor(Extractor):
    id = 'pdf-reader-extractor'
    name = 'PDF Reader Extractor'
    desc = 'Extracts words from a PDF file using PdfFileReader.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts information from a PDF File. This extractor only returns page
        counts.
        """
        try:
            from PyPDF2 import PdfFileReader
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            with open(file_path, 'rb') as file_:
                pdf_file = PdfFileReader(file_, strict=False)
                return {
                    Analysis.UNIT_TYPE_PAGES: pdf_file.getNumPages()
                }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)


