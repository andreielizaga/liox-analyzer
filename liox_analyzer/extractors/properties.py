# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class PropertiesExtractor(Extractor):
    id = 'properties-extractor'
    name = 'Property File Extractor'
    desc = 'Extracts text from a properties file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from a proprties file.
        """
        try:
            text = ''

            pattern = re.compile('^.*=(.*)$')

            with open(file_path, 'r') as file_:
                for line in file_.readlines():
                    match = re.search(pattern, line)
                    if match and line[0] != ';':
                        # skipping punctuation lines
                        text += ' ' + ' '.join(match.groups())

            text = remove_markup_tags(text)
            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)






