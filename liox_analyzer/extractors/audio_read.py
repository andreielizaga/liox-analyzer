# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class AudioReadExtractor(Extractor):
    id = 'audioread-extractor'
    name = 'AudioRead Extractor'
    desc = 'Extracts information from an audio file using the audioread lib'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Returns the duration of the supplied audio file.

        Raises a LioxFileAnalyzerException if audioread had an issue decoding
        the file or an ImportError if audioread isn't installed.
        """
        from ..utils import get_minutes

        try:
            import audioread
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [multimedia] extras.'
            )
            raise
        try:
            with audioread.audio_open(file_path) as file_:
                return {
                    Analysis.UNIT_TYPE_MINUTES: get_minutes(file_.duration)
                }
        except audioread.DecodeError:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
