# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from subprocess import PIPE, Popen
from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class CatdocExtractor(Extractor):
    id = 'catdoc-extractor'
    name = 'Catdoc Extractor'
    desc = 'Extracts text from a file using catdoc'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text from a word document using the catdoc program.        
        """
        try:
            with open(file_path, 'r') as file_:
                p = Popen([b'catdoc', b'w'], stdin=file_, stdout=PIPE, shell=True)
                output = p.communicate()[0]
            return {
                'text': output,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)

