# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import extract_from_markup


logger = logging.getLogger(__name__)


class PHPExtractor(Extractor):
    id = 'php-extractor'
    name = 'PHP File Extractor'
    desc = 'Extracts text from a PHP file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text from PHP file by stripping content inside "<? ?>" tags
        then using the HtmlExtractor to extract the texts.
        """
        try:
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()

            file_content = re.sub(r'(<\?)[^)]*\?>', '', file_content)
            text = extract_from_markup(file_content)

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
