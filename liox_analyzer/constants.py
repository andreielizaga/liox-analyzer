# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from itertools import chain

ARCHIVE_FILE_TYPES = ('story',)
AUDIO_FILE_TYPES = ('mp3', 'wav', 'm4a', 'wma', 'ogg',)
DOC_FILE_TYPES = ('doc',)
DOCX_FILE_TYPES = ('docx',)
EPUB_FILE_TYPES = ('epub',)
ERB_FILE_TYPES = ('erb',)
EXCEL_FILE_TYPES = ('xls', 'xlsx',)
HTML_FILE_TYPES = ('html', 'htm', 'flsnp',)
IDML_FILE_TYPES = ('idml',)
IMAGE_FILE_TYPES = ('png', 'jpg', 'jpeg', 'bmp',)
INX_FILE_TYPES = ('inx',)
JSON_FILE_TYPES = ('json', 'resjson',)
MIF_FILE_TYPES = ('mif',)
ODF_FILE_TYPES = ('odf', 'odt', 'ods', 'ott',)
PDF_FILE_TYPES = ('pdf',)
PO_FILE_TYPES = ('po', 'pot',)
POWERPOINT_FILE_TYPES = ('ppt', 'pptx',)
PROPERTIES_FILE_TYPES = ('properties', 'ini',)
PSD_FILE_TYPES = ('psd',)
RES_FILE_TYPES = ('resx', 'resw',)
RTF_FILE_TYPES = ('rtf',)
XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES = ('xml',)
XML_INCLUDE_NO_TRANSLATE_FILE_TYPES = ('sgml', 'flglo',)
SRT_FILE_TYPES = ('srt',)
STRINGS_FILE_TYPES = ('strings',)
TEXT_FILE_TYPES = ('txt', 'csv',)
TMX_FILE_TYPES = ('tmx',)
VIDEO_FILE_TYPES = ('mp4', 'mov', 'flv', 'wmv', 'm4v', 'avi', 'mpeg',)
VTT_FILE_TYPES = ('vtt',)
XLF_FILE_TYPES = ('xliff', 'xlf',)
YAML_FILE_TYPES = ('yml', 'yaml',)
ZIP_FILE_TYPES = ('zip',)
FLTOC_FILE_TYPES = ('fltoc',)
PHP_FILE_TYPES = ('php',)

BASIC_ANALYZER_FILE_TYPES = (
    DOC_FILE_TYPES,
    DOCX_FILE_TYPES,
    ERB_FILE_TYPES,
    TMX_FILE_TYPES,
    RES_FILE_TYPES,
    PO_FILE_TYPES,
    XLF_FILE_TYPES,
    XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES,
    XML_INCLUDE_NO_TRANSLATE_FILE_TYPES,
    ODF_FILE_TYPES,
    YAML_FILE_TYPES,
    PROPERTIES_FILE_TYPES,
    SRT_FILE_TYPES,
    STRINGS_FILE_TYPES,
    VTT_FILE_TYPES,
    MIF_FILE_TYPES,
    HTML_FILE_TYPES,
    IDML_FILE_TYPES,
    RTF_FILE_TYPES,
    INX_FILE_TYPES,
    PSD_FILE_TYPES,
    EPUB_FILE_TYPES,
    FLTOC_FILE_TYPES,
    PHP_FILE_TYPES,
)

ASIAN_LANGUAGES = {
    'ja-jp': 3,
    'th-th': 6,
    'zh-cn': 1.4,
    'zh-tw': 1.4,
    'zh-hk': 1.4,
}

SUPPORTED_TEXT_FILE_TYPES = list(chain(
    DOC_FILE_TYPES,
    DOCX_FILE_TYPES,
    XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES,
    XML_INCLUDE_NO_TRANSLATE_FILE_TYPES,
    STRINGS_FILE_TYPES,
    TEXT_FILE_TYPES,
    PO_FILE_TYPES,
    MIF_FILE_TYPES,
    INX_FILE_TYPES,
    EXCEL_FILE_TYPES,
    VTT_FILE_TYPES,
    YAML_FILE_TYPES,
    SRT_FILE_TYPES,
    XLF_FILE_TYPES,
    JSON_FILE_TYPES,
    HTML_FILE_TYPES,
    RTF_FILE_TYPES,
    IDML_FILE_TYPES,
    PROPERTIES_FILE_TYPES,
    RES_FILE_TYPES,
    PSD_FILE_TYPES,
    EPUB_FILE_TYPES,
    ERB_FILE_TYPES,
    TMX_FILE_TYPES,
    FLTOC_FILE_TYPES,
    ODF_FILE_TYPES,
    PHP_FILE_TYPES,
))

SUPPORTED_SLIDE_FILE_TYPES = list(chain(
    PDF_FILE_TYPES,
    POWERPOINT_FILE_TYPES,
))

SUPPORTED_VIDEO_FILE_TYPES = VIDEO_FILE_TYPES

SUPPORTED_AUDIO_FILE_TYPES = AUDIO_FILE_TYPES

SUPPORTED_ARCHIVE_FILE_TYPES = list(chain(
    ARCHIVE_FILE_TYPES,
    ZIP_FILE_TYPES,
))

XML_CONTENT_TYPES = list(chain(
    IDML_FILE_TYPES,
    INX_FILE_TYPES,
    RES_FILE_TYPES,
    XLF_FILE_TYPES,
    XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES,
))

SUPPORTED_CONTENT_TYPES = {
    'application/json': JSON_FILE_TYPES,
    'application/pdf': PDF_FILE_TYPES,
    'application/epub+zip': EPUB_FILE_TYPES,
    'application/rtf': RTF_FILE_TYPES,
    'application/xml': XML_CONTENT_TYPES,
    'application/vnd.mif': MIF_FILE_TYPES,
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': ['pptx'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['xlsx'],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': ['doc', 'docx'],
    'application/vnd.oasis.opendocument.text': ODF_FILE_TYPES,
    'application/zip': ZIP_FILE_TYPES,
    'image/jpeg': ['jpg', 'jpeg'],
    'image/png': ['png'],
    'image/vnd.adobe.photoshop': PSD_FILE_TYPES,
    'text/csv': ['csv'],
    'text/html': ['htm', 'html'],
    'text/plain': ['ini', 'po', 'pot', 'properties', 'srt', 'strings', 'txt', 'vtt'],
    'text/xml': XML_EXCLUDE_NO_TRANSLATE_FILE_TYPES,
    'text/yaml': YAML_FILE_TYPES,
    'video/mp4': ['mp4'],
    'video/quicktime': ['mov'],
    'video/x-flv': ['flv'],
    'video/x-m4v': ['m4v'],
    'video/x-ms-wmv': ['wmv'],
    'video/mpeg': ['mpeg']
}

WORDS_PER_MINUTE = 150
WORDS_PER_PAGE = 250
WORDS_PER_ROW = 250
WORDS_PER_SLIDE = 150
