import os
import sys

from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand


with open('README.rst') as f:
    readme = f.read()


multimedia = [
    'audioread==2.1.4',
    # Requires some system libs. See https://pypi.python.org/pypi/FFVideo.
    'FFVideo @ git+https://bitbucket.org/liox-ondemand/ffvideo.git@#egg=FFVideo-0.0.13',
]

documents = [
    'python-docx==0.8.6',
    'epub==0.5.2',
    'pdfminer==20131113',
    'psd-tools==0.8.4',
    'PyPDF2==1.26.0',
    'pyth==0.5.6',
    'python-pptx==0.6.2',
    'xlrd==1.1.0',
]

software = [
    'polib==1.0.3',
    'pyaml==14.05.7',
    'simplejson==3.10.0',
]

xml = [
    'BeautifulSoup4==4.5.0',
    'UCFlib==0.2.1',
]

extras = multimedia + documents + software + xml

setup(
    name='liox-analyzer',
    version='2.7.4',
    description='Lionbridge Technologies file analyzing tool',
    long_description=readme,
    url='https://bitbucket.org/liox-ondemand/liox-analyzer/',
    license='MIT License',
    author='Lionbridge Technologies, Inc.',
    author_email='service@lionbridge.com',
    packages = find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'analyzer_cli = liox_analyzer.__main__:main'
        ]
    },
    install_requires=[
        'nltk==3.0.3',  # Requires catdoc lib in system.
        'bleach~=2.1.2',
        'six~=1.10',
        'typing~=3.5',
    ],
    extras_require={
        'multimedia': multimedia,
        'documents': documents,
        'software': software,
        'xml': xml,
        'extras': extras,
        'dev': [
            'mypy>=0.471',
        ]
    },
    classifiers=(
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing',
    ),
)
